core = 7.x
api = 2

; uWaterloo configuration ISR study
projects[uw_cfg_isr][type] = "module"
projects[uw_cfg_isr][download][type] = "git"
projects[uw_cfg_isr][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_isr.git"
projects[uw_cfg_isr][download][tag] = "7.x-1.1"
projects[uw_cfg_isr][subdir] = ""
